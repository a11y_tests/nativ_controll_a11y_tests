//
//  AppDelegate.swift
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.11.2019.
//  Copyright © 2019 Артём Семёнов. All rights reserved.
//

import Cocoa

//@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

        var newWindow: NSWindow?
        var controller: ViewController?

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        let scRect = NSScreen.main!.frame
        let x = (scRect.width - 300)/2
        let y = (scRect.height - 300)/2
        let stile = NSWindow.StyleMask.init(arrayLiteral: [.titled, .closable])
        newWindow = NSWindow(contentRect: NSMakeRect(x, y, 300, 300), styleMask: stile, backing: .buffered, defer: false)
                controller = ViewController()
        let content = newWindow!.contentView! as NSView
        let view = controller!.view
        content.addSubview(view)
        newWindow?.title = "основное окошко"
        newWindow!.titleVisibility = .visible
        newWindow!.titlebarAppearsTransparent = true
        newWindow!.makeKeyAndOrderFront(newWindow!)
        newWindow!.makeFirstResponder(newWindow!.contentView?.subviews.last!)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}


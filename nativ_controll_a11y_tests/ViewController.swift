//
//  ViewController.swift
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.11.2019.
//  Copyright © 2019 Артём Семёнов. All rights reserved.
//

import Cocoa

class ViewController : NSViewController {
override func loadView() {
    let array = [
        "Bird", "Cat", "Dog", "Rabbit", "Pig"
    ]
    let view = CustomView.init(array: NSMutableArray.init(array: array), frame: NSMakeRect(0, 0, 300, 300))
    self.view = view
}

}


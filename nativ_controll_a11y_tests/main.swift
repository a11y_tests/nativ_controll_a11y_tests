//
//  Main.swift
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.11.2019.
//  Copyright © 2019 Артём Семёнов. All rights reserved.
//

import Foundation
import AppKit

let delegate = AppDelegate()
NSApplication.shared.delegate = delegate

print("код выхода \(NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv))")

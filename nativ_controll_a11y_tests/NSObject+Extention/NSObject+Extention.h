//
//  NSObject+Extention.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 11.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import <AppKit/AppKit.h>


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Extention)

@property(readonly) NSString *contents;
@property(readwrite) id a11yElement;

@end

NS_ASSUME_NONNULL_END

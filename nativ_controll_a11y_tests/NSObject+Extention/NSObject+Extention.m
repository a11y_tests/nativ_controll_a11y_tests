//
//  NSObject+Extention.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 11.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "NSObject+Extention.h"
#import <objc/runtime.h>
#import <AppKit/AppKit.h>


@implementation NSObject (Extention)
NSString const *key = @"a11y.element.key";

- (id)a11yElement {
    return objc_getAssociatedObject(self, &key);
}

- (void)setA11yElement:(id)a11yElement {
            objc_setAssociatedObject(self, &key, a11yElement, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)contents {return nil;}

@end

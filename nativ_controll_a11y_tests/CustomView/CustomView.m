//
//  CustomView.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.11.2019.
//  Copyright © 2019 Артём Семёнов. All rights reserved.
//

#import "CustomView.h"
#import "AccessibilityComponent.h"
#import <CoreGraphics/CoreGraphics.h>
#import <Foundation/Foundation.h>


@implementation CustomView

- (id) initWithArray: (NSMutableArray *) stringsArray frame: (NSRect) frame
{
    self = [self initWithFrame:frame]; // создание вью из фрейма
    // проверка существования массива
    if (stringsArray != nil) {
        staticList = [[StringList alloc] initWithArray:stringsArray];
        [CustomView initAxElements];
        heightListEtom = [NSFont systemFontSize] * 3 * 1.5; // высота одного пункта меню
        return self;
    }
    return NULL;
}

// реализация отрисовки списка

- (void)drawRect:(NSRect)dirtyRect
{
    [[NSColor windowBackgroundColor] set];
    NSRectFill([self bounds]);
    NSFont *font;
    NSColor *color = [NSColor textColor];
    NSPoint point = dirtyRect.origin;
    point.y = [NSFont systemFontSize] * 3 / 4;
    for (long i = [staticList count] - 1; i >= 0; i--) {
        if (i == [staticList numberSelectedElement]) {
            font = [NSFont systemFontOfSize:[NSFont systemFontSize] * 3 weight:NSFontWeightBold];
        } else {
            font = [NSFont systemFontOfSize:[NSFont systemFontSize] * 3];
        }
        NSMutableParagraphStyle *pStile = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
            pStile.alignment = NSTextAlignmentLeft;
        //    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
            NSDictionary *attrsDictionary = @{
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: color,
            NSParagraphStyleAttributeName: pStile,
                                             };
        [[[staticList contents] objectAtIndex:i] drawAtPoint:point withAttributes:attrsDictionary];
        point.y += heightListEtom;
    }
}

- (NSRect)getRectForListEtomWithNumber:(unsigned int)number
{
    CGFloat x = [[[NSApp windows] objectAtIndex:0] frame].origin.x;
    CGFloat y = [[[NSApp windows] objectAtIndex:0] frame].origin.y + ([staticList count] - 1 - number) * heightListEtom;
    return NSMakeRect(x, y, [self bounds].size.width, heightListEtom);
}

// реализация переключения пунктов меню

- (BOOL)acceptsFirstResponder {return YES;}
- (BOOL)canBecomeKeyView {return YES;};
- (BOOL)wantsLayer {return YES;}

- (void) upSelector
{
    printf("статический лист\n");
    for (NSString *elem in [staticList contents]) {
        printf("%s\n", [elem UTF8String]);
    }
    [staticList upSelector];
    [self display];
}

- (void) downSelector
{
    [staticList downSelector];
    [self display];
}

- (void)keyDown:(NSEvent *)event {

    if ([event keyCode] == 126) {
        [self downSelector];
    } else if ([event keyCode] == 125) {
        [self upSelector];
    } else {
        [super keyDown:event];
    }
    [AccessibilityComponent postFocusChanged:nil];
}

// реализация протокола NSAccessibilityTable

- (NSString *)accessibilityLabel {return @"Castom list";} // название кастомного листа

- (nonnull id)getAxData { 
    id ax = [[AccessibilityComponent alloc] initWithParent:self withAccessible:staticList withIndex:1 withView:self withRole:nil];
    return ax;
}

- (NSArray<NSAccessibilityAttributeName> *)accessibilityAttributeNames {
    return [[super accessibilityAttributeNames] arrayByAddingObject:NSAccessibilityChildrenAttribute];
}

- (id)accessibilityAttributeValue:(NSAccessibilityAttributeName)attribute {
    if ([attribute isEqualToString:NSAccessibilityChildrenAttribute]) {
        id result = NSAccessibilityUnignoredChildrenForOnlyChild([self getAxData]);
        return result;
    } else {
        return [super accessibilityAttributeValue:attribute];
    }
}

- (id)accessibilityHitTest:(NSPoint)point {
    id result = [[self getAxData] accessibilityHitTest:point];
    return result;
}

- (id)accessibilityFocusedUIElement {
    return [[self getAxData] accessibilityFocusedUIElement];
}

+ (void) initAxElements {
    int index = 0;
    AccessibilityComponent *list = [[AccessibilityComponent alloc] initWithParent:nil withAccessible:staticList withIndex:0 withView:nil withRole:NSAccessibilityListRole];
    [staticList setA11yElement:list];
    index++;
    for (id elem in [staticList contents]) {
        AccessibilityComponent *string = [[AccessibilityComponent alloc] initWithParent:nil withAccessible:elem withIndex:index withView:nil withRole:NSAccessibilityStaticTextRole];
        [elem setA11yElement:string];
        index++;
    }
}

@end

//
//  CustomView.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.11.2019.
//  Copyright © 2019 Артём Семёнов. All rights reserved.
//

#import <AppKit/AppKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomView : NSView
{
    CGFloat heightListEtom;
}

- (id) initWithArray: (NSMutableArray *) stringsArray frame: (NSRect) frame;
- (void) upSelector;
- (void) downSelector;
- (NSRect) getRectForListEtomWithNumber:(unsigned int)number;
- (id)getAxData;
+ (void) initAxElements;

@end

NS_ASSUME_NONNULL_END

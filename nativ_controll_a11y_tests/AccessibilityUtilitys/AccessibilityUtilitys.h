//
//  AccessibilityUtilitys.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 13.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import <AppKit/AppKit.h>


#import <Foundation/Foundation.h>


static SEL AccessibilityAttributeSetter(NSString *attribute);
static SEL AccessibilityAttributeSettableTester(NSString *attribute);
static NSString *AttributeWithoutAXPrefix(NSString *attribute);
static SEL AccessibilityAttributeGetter(NSString *attribute);
static BOOL AccessibilityIsSupportedAttribute(id element, NSString *attribute);
id AccessibilityAttributeValue(id element, NSString *attribute);
BOOL AccessibilityIsAttributeSettable(id element, NSString *attribute);
void AccessibilitySetAttributeValue(id element, NSString *attribute ,id value);

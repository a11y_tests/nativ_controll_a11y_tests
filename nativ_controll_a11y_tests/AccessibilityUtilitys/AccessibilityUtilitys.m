//
//  AccessibilityUtilitys.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 13.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityUtilitys.h"

#import <AppKit/AppKit.h>


static SEL AccessibilityAttributeSetter(NSString *attribute) {
    return NSSelectorFromString([NSString stringWithFormat:@"accessibilitySet%@Attribute:", AttributeWithoutAXPrefix(attribute)]);
}

static SEL AccessibilityAttributeSettableTester(NSString *attribute) {
    return NSSelectorFromString([NSString stringWithFormat:@"accessibilityIs%@AttributeSettable", AttributeWithoutAXPrefix(attribute)]);
}

static NSString *AttributeWithoutAXPrefix(NSString *attribute) {
    return [attribute hasPrefix:@"AX"] ? [attribute substringFromIndex:2] : attribute;
}

static SEL AccessibilityAttributeGetter(NSString *attribute) {
     return NSSelectorFromString([NSString stringWithFormat:@"accessibility%@Attribute", AttributeWithoutAXPrefix(attribute)]);
}

static BOOL AccessibilityIsSupportedAttribute(id element, NSString *attribute) {
    return [[element accessibilityAttributeNames] indexOfObject:attribute] != NSNotFound;
}

id AccessibilityAttributeValue(id element, NSString *attribute) {
        if (!AccessibilityIsSupportedAttribute(element, attribute)) return nil;
        SEL getter = AccessibilityAttributeGetter(attribute);
        return [element performSelector:getter];
}

BOOL AccessibilityIsAttributeSettable(id element, NSString *attribute) {
        if (!AccessibilityIsSupportedAttribute(element, attribute)) return NO;
        SEL tester = AccessibilityAttributeSettableTester(attribute);
        return [element performSelector:tester] != nil;
}

void AccessibilitySetAttributeValue(id element, NSString *attribute ,id value) {
        if (!AccessibilityIsSupportedAttribute(element, attribute)) return;
        SEL setter = AccessibilityAttributeSetter(attribute);
        if (![element accessibilityIsAttributeSettable:attribute]) return;
        [element performSelector:setter withObject:value];
}

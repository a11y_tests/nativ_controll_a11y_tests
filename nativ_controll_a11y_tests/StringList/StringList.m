//
//  StringList.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 10.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "StringList.h"
#import "AccessibilityComponent.h"

@implementation StringList

- (long)numberSelectedElement {return numberOfSelectedElement;}
- (NSArray *)contents {
    return listStrings;
}

- (NSString *)selectedElement {return [listStrings objectAtIndex:numberOfSelectedElement];}
- (long)count {return [listStrings count];}

- (id)initWithArray:(NSArray<NSString *> *)listOfStrings {
    if (listOfStrings != nil) {
        listStrings = [NSArray arrayWithArray:listOfStrings];
        numberOfSelectedElement = 0;
        return self;
    } else return nil;
}

- (void)upSelector {
            printf("upSelector() 1\n");
    if ([self a11yElement] != nil) {
        printf("upSelector() 2\n");
        [(AccessibilityComponent *)[self a11yElement] postValueChanged];
        [(AccessibilityComponent *)[self a11yElement] postSelectionChanged];
    }
    AccessibilityComponent *comp = [self a11yElement];
    if (numberOfSelectedElement == [listStrings count] - 1) {
        numberOfSelectedElement = 0;
    } else numberOfSelectedElement++;
}

- (void)downSelector {
    if ([self a11yElement] != nil) {
        [(AccessibilityComponent *)[self a11yElement] postValueChanged];
        [(AccessibilityComponent *)[self a11yElement] postSelectionChanged];
    }
    if (numberOfSelectedElement == 0) {
        numberOfSelectedElement = [listStrings count] - 1;
    } else numberOfSelectedElement--;
}

@end

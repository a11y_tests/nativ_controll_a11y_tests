//
//  StringList.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 10.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Extention.h"

@interface StringList : NSObject {
        NSArray *listStrings;
    long numberOfSelectedElement;
}

@property(readonly)long numberSelectedElement;
@property(readonly) NSArray* contents;
@property(readonly) NSString* selectedElement;
@property(readonly) long count;
- (id)initWithArray:(NSArray<NSString *> *)listOfStrings;
- (void)upSelector;
- (void)downSelector;

@end

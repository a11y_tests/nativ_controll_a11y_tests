//
//  AccessibilityTabGroupControl.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityTabGroupControl.h"
#import "AccessibilityUtilitys.h"

@implementation AccessibilityTabGroupControl

- (id)initWithParent:(NSObject *)parent withAccessible:(id)accessible withIndex:(int)index withTabGroup:(id)tabGroup withView:(NSView *)view withJavaRole:(NSAccessibilityRole)javaRole {
    self = [super initWithParent:parent withAccessible:accessible withIndex:index withView:view withRole:javaRole];
    if ((self)) {
        if (tabGroup != NULL) {
            fTabGroup = tabGroup;
        }
    }
    return self;
}

- (id)tabGroup {
    return fTabGroup;
}

- (void)getActionsWithEnv {
    
}

- (id)accessibilityValueAttribute {
    return [NSNumber numberWithBool:selected];
}

@end

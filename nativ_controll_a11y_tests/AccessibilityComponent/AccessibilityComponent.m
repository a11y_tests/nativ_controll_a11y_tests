//
//  AccessibilityComponent.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 28.01.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityComponent.h"
#import "AccessibilityText.h"
#import "AccessibilityUtilitys.h"

@implementation AccessibilityComponent

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@(title:'%@', desc:'%@', value:'%@')", [self accessibilityRoleAttribute],
        [self accessibilityTitleAttribute], [self accessibilityRoleDescriptionAttribute], [self accessibilityValueAttribute]];
}

- (id)initWithParent:(NSObject *)parent withAccessible:(id)accessible withIndex:(int)index withView:(NSView *)view withRole:(NSAccessibilityRole)javaRole {
    self = [ super init];
    if (self) {
        fParent = parent;
        fView = [fView self];
        fComponent = accessible;
        fIndex = index;
        fActions = nil;
        fActionsLOCK = [[NSObject alloc] init];
        if (javaRole == nil) {
            if ([accessible isKindOfClass:[StringList class]]) {
                fRole = NSAccessibilityListRole;
            } else if ([accessible isKindOfClass:[NSString class]]) {
                fRole = NSAccessibilityStaticTextRole;
            } else fRole = nil;
        } else fRole = javaRole;
    }
    return self;
}

- (void)postValueChanged { 
    NSAccessibilityPostNotification(self, NSAccessibilityValueChangedNotification);
}

- (void)postSelectedTextChanged { 
    NSAccessibilityPostNotification(self, NSAccessibilitySelectedTextChangedNotification);
}

- (void)postSelectionChanged { 
    NSAccessibilityPostNotification(self, NSAccessibilitySelectedChildrenChangedNotification);
}

- (BOOL)isEqual:(id)anObject { 
    if (![anObject isKindOfClass:[self class]]) return NO;
    else return YES;
}

+ (void)postFocusChanged:(id)message { 
    NSAccessibilityPostNotification([NSApp accessibilityFocusedUIElement], NSAccessibilityFocusedUIElementChangedNotification);
}

+ (NSArray *)childrenOfParent:(AccessibilityComponent *)parent withChildrenCode:(NSInteger)childrenCode {
    if (parent->fComponent == NULL) return nil;
    if ([[parent->fComponent contents] count] == 0) return nil;
    long count = [[parent->fComponent contents] count];
    if (childrenCode >= count) return nil;
    else if (childrenCode >= 0) {
        id object = [[parent->fComponent contents] objectAtIndex:childrenCode];
        AccessibilityComponent *ax = nil;
        if ([object isKindOfClass:[NSString class]]) {
            ax = [AccessibilityComponent createWithParent:parent accessible:object role:NSAccessibilityStaticTextRole withView:[parent view]];
        } else if ([object isKindOfClass:[StringList class]]) {
            ax = [AccessibilityComponent createWithParent:parent accessible:object role:NSAccessibilityListRole withView:[parent view]];
        }
        if ([parent->fComponent isKindOfClass:[StringList class]]) {
            StringList *list = (StringList *)parent->fComponent;
            if (childrenCode == [list numberSelectedElement]) [ax setIsSelected:YES];
            else [ax setIsSelected:NO];
        }
        [ax setIsVisible:YES];
        return [NSArray arrayWithObject:ax];
    } else {
                                 NSMutableArray *childrens = [[NSMutableArray alloc] initWithCapacity:count];
    for (long i = 0; i < count; i++) {
        id object = [[parent->fComponent contents] objectAtIndex:i];
        AccessibilityComponent *ax = nil;
        if ([object isKindOfClass:[NSString class]]) {
            ax = [AccessibilityComponent createWithParent:parent accessible:object role:NSAccessibilityStaticTextRole withView:[parent view]];
        } else if ([object isKindOfClass:[StringList class]]) {
            ax = [AccessibilityComponent createWithParent:parent accessible:object role:NSAccessibilityListRole withView:[parent view]];
        }
        if ([parent->fComponent isKindOfClass:[StringList class]]) {
            StringList *list = (StringList *)parent->fComponent;
            if (i == [list numberSelectedElement]) [ax setIsSelected:YES];
            else [ax setIsSelected:NO];
        }
        [ax setIsVisible:YES];
        if (childrenCode == AX_ALL_CHILDREN) {
            [childrens addObject:ax];
        } else if ((childrenCode == AX_VISIBLE_CHILDREN) && (ax.isVisible)) [childrens addObject:ax];
        else if ((childrenCode == AX_SELECTED_CHILDREN) && (ax.isSelected)) [childrens addObject:ax];
    }
    return [NSArray arrayWithArray:childrens];
}
}

+ (AccessibilityComponent *)createWithParent:(AccessibilityComponent *)parent accessible:(id)jaccessible role:(NSAccessibilityRole)javaRole withView:(NSView *)view {
    AccessibilityComponent *children = nil;
    if ([javaRole isEqualToString:NSAccessibilityStaticTextRole] || [javaRole isEqualToString:NSAccessibilityTextAreaRole] || [javaRole isEqualToString:NSAccessibilityTextFieldRole]) {
        children = [AccessibilityText alloc];
    } else {
        children = [AccessibilityComponent alloc];
    }
    [children initWithParent:parent withAccessible:jaccessible withIndex:[[parent->fComponent contents] count] withView:view withRole:javaRole];
    [children->fComponent setA11yElement:children];
     return children;
}

+ (AccessibilityComponent *)createWithAccessible:(id)jaccessible role:(NSAccessibilityRole)role withView:(NSView *)view {
    return [self createWithParent:nil accessible:jaccessible role:role withView:view];
}

- (NSDictionary *)getActions {
    @synchronized(fActionsLOCK) {
    if (fActions == nil) {
        fActions = [[NSMutableDictionary alloc] initWithCapacity:3];
        [self getActionsWithEnv];
    }
    }
    return fActions;
}

- (void)getActionsWithEnv { 
    
}

- (NSView *)view { 
    return fView;
}

- (NSWindow *)window { 
    return [[self view] window];
}

- (id)parent { 
    if (fParent == nil) {
        fParent = fView;
    }
    return fParent;
}

- (NSAccessibilityRole)javaRole {
    return fRole;
}

- (BOOL)isMenu { 
    id role = [self accessibilityRoleAttribute];
    return [role isEqualToString:NSAccessibilityMenuBarRole] || [role isEqualToString:NSAccessibilityMenuRole] || [role isEqualToString:NSAccessibilityMenuItemRole];
}

- (NSString *)accessibilityRoleAttribute { 
    return fRole;
}

- (BOOL)isSelected {
    return selected;
}

- (void)setIsSelected:(BOOL)isSelected {
    selected = isSelected;
}

- (BOOL)isSelectable {
    return selectible;
}

- (void)setIsSelectable:(BOOL)isSelectable {
    selectible = isSelectable;
}

- (BOOL)isVisible {
    return visible;
}

- (void)setIsVisible:(BOOL)isVisible {
    visible = isVisible;
}

- (NSArray *)initializeAttributeNamesWithEnv { 
    NSMutableArray *attributeNames = [NSMutableArray arrayWithCapacity:20];

    // все элементы подходят к  родителю, роли, описанию роли, окну, topLevelUIElement, справке
    [attributeNames addObject:NSAccessibilityParentAttribute];
    [attributeNames addObject:NSAccessibilityRoleAttribute];
    [attributeNames addObject:NSAccessibilityRoleDescriptionAttribute];
    [attributeNames addObject:NSAccessibilityHelpAttribute];

    [attributeNames addObject:NSAccessibilityWindowAttribute];
    [attributeNames addObject:NSAccessibilityTopLevelUIElementAttribute];

    if (fRole == NSAccessibilitySecureTextFieldSubrole) {
        [attributeNames addObject:NSAccessibilitySubroleAttribute];
    }
    [attributeNames addObject:NSAccessibilityEnabledAttribute];
    [attributeNames addObject:NSAccessibilitySizeAttribute];
    [attributeNames addObject:NSAccessibilityPositionAttribute];
        [attributeNames addObject:NSAccessibilityFocusedAttribute];

    if (fRole == NSAccessibilityRadioButtonRole) {
        [attributeNames addObject:NSAccessibilityValueAttribute];
    } else {
        if (fRole != NSAccessibilityButtonRole) {
            [attributeNames addObject:NSAccessibilityValueAttribute];
            [attributeNames addObject:NSAccessibilityMinValueAttribute];
            [attributeNames addObject:NSAccessibilityMaxValueAttribute];
        }
    }
    [attributeNames addObject:NSAccessibilityOrientationAttribute];
    [attributeNames addObject:NSAccessibilityTitleAttribute];
    if ([AccessibilityComponent childrenOfParent:self withChildrenCode:AX_ALL_CHILDREN] != nil) {
            [attributeNames addObject:NSAccessibilityChildrenAttribute];
    }
    if (fRole == NSAccessibilityListRole) {
            [attributeNames addObject:NSAccessibilitySelectedChildrenAttribute];
            [attributeNames addObject:NSAccessibilityVisibleChildrenAttribute];
    }

    return attributeNames;
    ;
}

- (NSArray *)accessibilityAttributeNames { 
    NSArray *names = [self initializeAttributeNamesWithEnv];
    if ([self accessibilityIsIgnored]) {
        return names;
    }
    id myParent = [self accessibilityParentAttribute];
        if ([myParent isKindOfClass:[AccessibilityComponent class]]) {
                NSString *parentRole = [(AccessibilityComponent *)myParent javaRole];
                if ([parentRole isEqualToString:NSAccessibilityListRole]) {
                    NSMutableArray *moreNames =
                        [[NSMutableArray alloc] initWithCapacity: [names count] + 2];
                    [moreNames addObjectsFromArray: names];
                    [moreNames addObject:NSAccessibilitySelectedAttribute];
                    [moreNames addObject:NSAccessibilityIndexAttribute];
                    return moreNames;
                }
            }
            // popupmenu's return values not selected children
            if ( [fRole isEqualToString:NSAccessibilityMenuRole] &&
                 ![[[self parent] javaRole] isEqualToString:NSAccessibilityMenuButtonRole] ) {
                NSMutableArray *moreNames =
                    [[NSMutableArray alloc] initWithCapacity: [names count] + 1];
                [moreNames addObjectsFromArray: names];
                [moreNames addObject:NSAccessibilityValueAttribute];
                return moreNames;
            }
            return names;
}

- (NSArray *)accessibilityChildrenAttribute { 
    NSArray *children = [AccessibilityComponent childrenOfParent:self withChildrenCode:AX_ALL_CHILDREN];

    NSArray *value = nil;
    if ([children count] > 0) {
        value = children;
    }

    return value;
}

- (BOOL)accessibilityIsChildrenAttributeSettable { 
    return NO;
}

- (NSUInteger)accessibilityIndexOfChild:(id)child { 
        if (![[self accessibilityRoleAttribute] isEqualToString:NSAccessibilityListRole]) {
        return [super accessibilityIndexOfChild:child];
    }

    return [[AccessibilityComponent childrenOfParent:self withChildrenCode:AX_SELECTED_CHILDREN] indexOfObject:child];
}

- (NSArray *)accessibilityArrayAttributeValues:(NSString *)attribute index:(NSUInteger)index maxCount:(NSUInteger)maxCount { 
        if ( (maxCount == 1) && [attribute isEqualToString:NSAccessibilityChildrenAttribute]) {
        // Children codes for ALL, SELECTED, VISIBLE are <0. If the code is >=0, we treat it as an index to a single child
        NSArray *child = [AccessibilityComponent childrenOfParent:self withChildrenCode:(NSInteger)index];
        if ([child count] > 0) {
            return child;
        }
    }
    return [super accessibilityArrayAttributeValues:attribute index:index maxCount:maxCount];;
}

- (NSNumber *)accessibilityEnabledAttribute {
    if ([self accessibilityIsIgnored]) {
        return nil;
    } else {
        return [[NSNumber alloc] initWithInt:1];
    }
}

- (BOOL)accessibilityIsEnabledAttributeSettable { 
    return NO;
}

- (NSNumber *)accessibilityFocusedAttribute { 
        if ([self accessibilityIsFocusedAttributeSettable]) {
        return [NSNumber numberWithBool:[self isEqual:[NSApp accessibilityFocusedUIElement]]];
    }
    return [NSNumber numberWithBool:NO];;
}

- (BOOL)accessibilityIsFocusedAttributeSettable {
    return isFocused;
}

- (void)accessibilitySetFocusedAttribute:(id)value { 
    if ([(NSNumber *)value boolValue]) {
        isFocused = [(NSNumber *)value boolValue];
    }
}

- (NSString *)accessibilityHelpAttribute { 
    return @"help";
}

- (BOOL)accessibilityIsHelpAttributeSettable { 
    return NO;
}

- (NSValue *)accessibilityIndexAttribute { 
        NSInteger index = fIndex;
    NSValue *returnValue = [NSValue value:&index withObjCType:@encode(NSInteger)];
    return returnValue;
}

- (BOOL)accessibilityIsIndexAttributeSettable { 
    return NO;
}

- (id)accessibilityMaxValueAttribute { 
    return [[AccessibilityComponent childrenOfParent:self withChildrenCode:AX_ALL_CHILDREN] lastObject];
}

- (BOOL)accessibilityIsMaxValueAttributeSettable { 
    return NO;
}

- (BOOL)accessibilityIsMinValueAttributeSettable { 
    return NO;
}

- (id)accessibilityMinValueAttribute { 
    return [[AccessibilityComponent childrenOfParent:self withChildrenCode:AX_ALL_CHILDREN] firstObject];
}

- (BOOL)accessibilityIsOrientationAttributeSettable { 
    return NO;
}

- (id)accessibilityOrientationAttribute {
    return orientation;
}

- (BOOL)accessibilityIsParentAttributeSettable { 
    return NO;
}

- (id)accessibilityParentAttribute { 
    return NSAccessibilityUnignoredAncestor([self parent]);
}

- (BOOL)accessibilityIsPositionAttributeSettable { 
    return NO;
}

- (NSValue *)accessibilityPositionAttribute {
    NSSize size = fFrame.size;
        NSPoint point = fFrame.origin;

    point.y += size.height;

    point.y = [[[[self view] window] screen] frame].size.height - point.y;

    return [NSValue valueWithPoint:point];;
}

- (BOOL)accessibilityIsRoleAttributeSettable { 
    return NO;
}

- (BOOL)accessibilityIsRoleDescriptionAttributeSettable { 
    return NO;
}

- (NSString *)accessibilityRoleDescriptionAttribute { 
    NSString *value = NSAccessibilityRoleDescription([self accessibilityRoleAttribute], nil);
    if (value == nil) {
        value = @"unknown";
    }
    return value;
}

- (BOOL)accessibilityIsSelectedChildrenAttributeSettable { 
    return NO;
}

- (NSArray *)accessibilitySelectedChildrenAttribute {
    // получаем колво выбранных элементов
    NSArray *selectedChildrens = [AccessibilityComponent childrenOfParent:self withChildrenCode:AX_SELECTED_CHILDREN];
    if ([selectedChildrens count] == 0) {
        return nil;
    } else return selectedChildrens;
}

- (BOOL)accessibilityIsSelectedAttributeSettable { 
    return [self isSelected];
}

- (NSNumber *)accessibilitySelectedAttribute { 
    return [NSNumber numberWithBool:[self isSelected]];
}

- (BOOL)accessibilityIsSizeAttributeSettable { 
    return NO;
}

- (NSValue *)accessibilitySizeAttribute { 
    return [NSValue valueWithSize:fFrame.size];
}

- (BOOL)accessibilityIsSubroleAttributeSettable { 
    return NO;
}

- (NSString *)accessibilitySubroleAttribute { 
    return fRole;
}

- (BOOL)accessibilityIsTitleAttributeSettable { 
    return NO;
}

- (NSString *)accessibilityTitleAttribute { 
        if ([[self accessibilityRoleAttribute] isEqualToString:NSAccessibilityStaticTextRole]) {
        return (NSString *)fComponent;
    } else return @"";
}

- (BOOL)accessibilityIsTopLevelUIElementAttributeSettable { 
    return NO;
}

- (NSWindow *)accessibilityTopLevelUIElementAttribute { 
    return [self window];
}

- (BOOL)accessibilityIsValueAttributeSettable { 
        BOOL isSettable = NO;
    NSString *role = [self accessibilityRoleAttribute];

    if ([role isEqualToString:NSAccessibilityScrollBarRole] || // according to NSScrollerAccessibility
        [role isEqualToString:NSAccessibilitySplitGroupRole] ) // according to NSSplitViewAccessibility
    {
        isSettable = YES;
    }
    return isSettable;
;
}

- (id)accessibilityValueAttribute {
    if ([fComponent isKindOfClass:[NSString class]]) {
        return fComponent;
    } else return [[self accessibilitySelectedChildrenAttribute] objectAtIndex:[staticList numberSelectedElement]];
}

- (BOOL)accessibilityIsVisibleChildrenAttributeSettable { 
    return NO;
}

- (NSArray *)accessibilityVisibleChildrenAttribute { 
    NSArray *visibleChildrens = [AccessibilityComponent childrenOfParent:self withChildrenCode:AX_VISIBLE_CHILDREN];
    return visibleChildrens;
}

- (BOOL)accessibilityIsWindowAttributeSettable { 
    return NO;
}

- (id)accessibilityWindowAttribute { 
    return [self window];
}

- (id)accessibilityFocusedUIElement { 
        id value = nil;

    value = [[AccessibilityComponent childrenOfParent:self withChildrenCode:AX_SELECTED_CHILDREN] firstObject];

    if (value == nil) {
        value = self;
    }
        return value;
}

- (id)accessibilityHitTest:(NSPoint)point { 
    id value = nil;
    if (value == nil) {
        value = self;
    }

    if ([value accessibilityIsIgnored]) {
        value = NSAccessibilityUnignoredAncestor(value);
    }
    return value;
}

- (NSArray *)accessibilityActionNames { 
    return nil;
}

- (NSString *)accessibilityActionDescription:(NSString *)action { 
    return nil;
}

- (void)accessibilityPerformAction:(NSString *)action { 
    
}

- (BOOL)isAccessibleWithEnv {
    return ![self accessibilityIsIgnored];
}

- (BOOL)accessibilityIsIgnored {return NO;}

- (id)accessibilityAttributeValue:(NSString *)attribute { 
    return AccessibilityAttributeValue(self, attribute);
}

- (void)accessibilitySetValue:(id)value forAttribute:(NSString *)attribute {
    if ([self accessibilityIsAttributeSettable:attribute]) {
        // calls setter on self
        AccessibilitySetAttributeValue(self, attribute, value);
    }
}

- (BOOL)accessibilityIsAttributeSettable:(NSString *)attribute { 
    return AccessibilityIsAttributeSettable(self, attribute);
}

@end

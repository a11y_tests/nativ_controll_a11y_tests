//
//  AccessibilityTabGroup.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityTabGroup.h"
#import "AccessibilityUtilitys.h"

@implementation AccessibilityTabGroup

- (id)currentTabWithEnv {
    NSArray *tabs = [self tabControlsWithEnv];
    _numTabs = [tabs count];
    AccessibilityComponent *aTab;
    NSInteger i;
    for (i = 0; i < _numTabs; i++) {
        aTab = (AccessibilityComponent *)[tabs objectAtIndex:i];
        if ([aTab isAccessibleWithEnv]) {
            return aTab;
        }
    }
    return nil;
}

- (NSArray *)tabControlsWithEnv {
    return nil;
}

- (NSArray *)contentsWithEnv {
        id currentTab = [self currentTabWithEnv];
    if (currentTab == nil) return nil;

    NSArray *contents = [AccessibilityComponent childrenOfParent:currentTab withChildrenCode:AX_ALL_CHILDREN];
    if ([contents count] <= 0) return nil;
    return contents;
}

- (NSArray *)initializeAttributeNamesWithEnv {
    NSMutableArray *names = (NSMutableArray *)[super initializeAttributeNamesWithEnv];

    [names addObject:NSAccessibilityTabsAttribute];
    [names addObject:NSAccessibilityContentsAttribute];
    [names addObject:NSAccessibilityValueAttribute];

    return names;
}

- (NSArray *)accessibilityArrayAttributeValues:(NSString *)attribute index:(NSUInteger)index maxCount:(NSUInteger)maxCount {
    NSArray *result = nil;
    if ( (maxCount == 1) && [attribute isEqualToString:NSAccessibilityChildrenAttribute]) {

        NSArray *children = [self tabControlsWithEnv];
        if ([children count] > 0) {
            result = children;
         } else {
            children = [self contentsWithEnv];
            if ([children count] > 0) {
                result = children;
            }
        }
    } else {
        result = [super accessibilityArrayAttributeValues:attribute index:index maxCount:maxCount];
    }
    return result;
}

- (NSArray *)accessibilityChildrenAttribute {
    NSArray *tabs = [self accessibilityTabsAttribute];
    NSArray *contents = [self accessibilityContentsAttribute];

    NSMutableArray *children = [NSMutableArray arrayWithCapacity:[tabs count] + [contents count]];
    [children addObjectsFromArray:tabs];
    [children addObjectsFromArray:contents];

    return (NSArray *)children;
}

- (id)accessibilityTabsAttribute {
        id tabs = [self tabControlsWithEnv];
    return tabs;;
}

- (BOOL)accessibilityIsTabsAttributeSettable {
    return NO;
}

- (NSArray *)accessibilityContentsAttribute {
        NSArray* cont = [self contentsWithEnv];
    return cont;
}

- (BOOL)accessibilityIsContentsAttributeSettable {
    return NO;
}
- (id)accessibilityValueAttribute {
        id val = [self currentTabWithEnv];
    return val;
}

@end

//
//  AccessibilityScrollArea.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 05.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityScrollArea.h"
#import "AccessibilityUtilitys.h"

@implementation AccessibilityScrollArea



- (NSArray *)initializeAttributeNamesWithEnv {
    NSMutableArray *names = (NSMutableArray *)[super initializeAttributeNamesWithEnv];
    [names addObject:NSAccessibilityHorizontalScrollBarAttribute];
    [names addObject:NSAccessibilityVerticalScrollBarAttribute];
    [names addObject:NSAccessibilityContentsAttribute];
    return names;
}

- (NSArray *)accessibilityContentsAttribute {
    NSArray *children = [AccessibilityComponent childrenOfParent:self withChildrenCode:AX_ALL_CHILDREN];
    if ([children count] <= 0) return nil;
    NSArray *contents = [NSMutableArray arrayWithCapacity:[children count]];

    // The scroll bars are in the children. children less the scroll bars is the contents
    NSEnumerator *enumerator = [children objectEnumerator];
    AccessibilityComponent *aElement;
    while ((aElement = (AccessibilityComponent *)[enumerator nextObject])) {
        if (![[aElement accessibilityRoleAttribute] isEqualToString:NSAccessibilityScrollBarRole]) {
            // no scroll bars in contents
            [(NSMutableArray *)contents addObject:aElement];
        }
    }

    return contents;
}

- (BOOL)accessibilityIsContentsAttributeSettable {
    return NO;
}

- (BOOL)accessibilityIsVerticalScrollBarAttributeSettable {
    return NO;
}

- (BOOL)accessibilityIsHorizontalScrollBarAttributeSettable {
    return NO;
}

- (id)accessibilityHorizontalScrollBarAttribute {
    NSArray *children = [AccessibilityComponent childrenOfParent:self withChildrenCode:AX_SELECTED_CHILDREN];
    if ([children count] <= 0) return nil;

    // The scroll bars are in the children.
    AccessibilityComponent *aElement;
    NSEnumerator *enumerator = [children objectEnumerator];
    while (aElement = (AccessibilityComponent *)[enumerator nextObject]) {
        if ([[aElement accessibilityRoleAttribute] isEqualToString:NSAccessibilityScrollBarRole]) {
                return aElement;
        }
    }

    return nil;
}

- (id)accessibilityVerticalScrollBarAttribute {
    NSArray *children = [AccessibilityComponent childrenOfParent:self withChildrenCode:AX_ALL_CHILDREN];
    if ([children count] <= 0) return nil;

    // The scroll bars are in the children.
    NSEnumerator *enumerator = [children objectEnumerator];
    AccessibilityComponent *aElement;
    while ((aElement = (AccessibilityComponent *)[enumerator nextObject])) {
        if ([[aElement accessibilityRoleAttribute] isEqualToString:NSAccessibilityScrollBarRole]) {
                return aElement;
        }
    }

    return nil;
}

@end

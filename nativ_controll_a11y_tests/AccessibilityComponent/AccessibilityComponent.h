//
//  AccessibilityComponent.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 28.01.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StringList.h"
#import "NSObject+Extention.h"
#import <AppKit/AppKit.h>

// коды выборки детей как в джаве
#define AX_ALL_CHILDREN (-1)
#define AX_SELECTED_CHILDREN (-2)
#define AX_VISIBLE_CHILDREN (-3)

static StringList *staticList;

@interface AccessibilityComponent : NSObject {
    NSView *fView;
    NSObject *fParent;
    id fComponent;
    int fIndex;
    NSMutableDictionary *fActions;
    NSObject *fActionsLOCK;
    NSAccessibilityRole fRole;
    bool selected, selectible, visible, isFocused;
    NSAccessibilityOrientationValue orientation;
    NSRect fFrame;
}

- (id)initWithParent:(NSObject*)parent withAccessible:(id)accessible withIndex:(int)index withView:(NSView *)view withRole:(NSAccessibilityRole)javaRole;
- (void)postValueChanged;
- (void)postSelectedTextChanged;
- (void)postSelectionChanged;
- (BOOL)isEqual:(id)anObject;
+ (void)postFocusChanged:(id)message;
+ (NSArray*)childrenOfParent:(AccessibilityComponent*)parent withChildrenCode:(NSInteger)childrenCode;
+ (AccessibilityComponent *)createWithParent:(AccessibilityComponent *)parent accessible:(id)jaccessible role:(NSAccessibilityRole)javaRole withView:(NSView *)view;
+ (AccessibilityComponent *) createWithAccessible:(id)jaccessible role:(NSAccessibilityRole)role withView:(NSView *)view;
- (NSDictionary*)getActions;
- (void)getActionsWithEnv;
- (NSView*)view;
- (NSWindow*)window;
- (id)parent;
- (NSAccessibilityRole)javaRole;
- (BOOL)isMenu;
@property(readwrite) BOOL isSelected;
@property(readwrite) BOOL isSelectable;
@property(readwrite) BOOL isVisible;
@property(readonly) BOOL isAccessibleWithEnv;

// атрибуты
- (NSArray *)initializeAttributeNamesWithEnv;
- (NSArray *)accessibilityAttributeNames;

- (id)accessibilityAttributeValue:(NSString *)attribute;
- (BOOL)accessibilityIsAttributeSettable:(NSString *)attribute;
- (void)accessibilitySetValue:(id)value forAttribute:(NSString *)attribute;

- (NSArray *)accessibilityChildrenAttribute;
- (BOOL)accessibilityIsChildrenAttributeSettable;
- (NSUInteger)accessibilityIndexOfChild:(id)child;
- (NSNumber *)accessibilityEnabledAttribute;
- (BOOL)accessibilityIsEnabledAttributeSettable;
- (NSNumber *)accessibilityFocusedAttribute;
- (BOOL)accessibilityIsFocusedAttributeSettable;
- (void)accessibilitySetFocusedAttribute:(id)value;
- (NSString *)accessibilityHelpAttribute;
- (BOOL)accessibilityIsHelpAttributeSettable;
- (NSValue *)accessibilityIndexAttribute;
- (BOOL)accessibilityIsIndexAttributeSettable;
- (id)accessibilityMaxValueAttribute;
- (BOOL)accessibilityIsMaxValueAttributeSettable;
- (id)accessibilityMinValueAttribute;
- (BOOL)accessibilityIsMinValueAttributeSettable;
- (NSArray *)accessibilityArrayAttributeValues:(NSString *)attribute index:(NSUInteger)index maxCount:(NSUInteger)maxCount;
- (id)accessibilityOrientationAttribute;
- (BOOL)accessibilityIsOrientationAttributeSettable;
- (id)accessibilityParentAttribute;
- (BOOL)accessibilityIsParentAttributeSettable;
- (NSValue *)accessibilityPositionAttribute;
- (BOOL)accessibilityIsPositionAttributeSettable;
- (NSString *)accessibilityRoleAttribute;
- (BOOL)accessibilityIsRoleAttributeSettable;
- (NSString *)accessibilityRoleDescriptionAttribute;
- (BOOL)accessibilityIsRoleDescriptionAttributeSettable;
- (NSArray *)accessibilitySelectedChildrenAttribute;
- (BOOL)accessibilityIsSelectedChildrenAttributeSettable;
- (NSNumber *)accessibilitySelectedAttribute;
- (BOOL)accessibilityIsSelectedAttributeSettable;
- (void)accessibilitySetSelectedAttribute:(id)value;
- (NSValue *)accessibilitySizeAttribute;
- (BOOL)accessibilityIsSizeAttributeSettable;
- (NSString *)accessibilitySubroleAttribute;
- (BOOL)accessibilityIsSubroleAttributeSettable;
- (NSString *)accessibilityTitleAttribute;
- (BOOL)accessibilityIsTitleAttributeSettable;
- (NSWindow *)accessibilityTopLevelUIElementAttribute;
- (BOOL)accessibilityIsTopLevelUIElementAttributeSettable;
- (id)accessibilityValueAttribute;
- (BOOL)accessibilityIsValueAttributeSettable;
- (void)accessibilitySetValueAttribute:(id)value;
- (NSArray *)accessibilityVisibleChildrenAttribute;
- (BOOL)accessibilityIsVisibleChildrenAttributeSettable;
- (id)accessibilityWindowAttribute;
- (BOOL)accessibilityIsWindowAttributeSettable;

- (NSArray *)accessibilityActionNames;
- (NSString *)accessibilityActionDescription:(NSString *)action;
- (void)accessibilityPerformAction:(NSString *)action;

- (id)accessibilityHitTest:(NSPoint)point;
- (id)accessibilityFocusedUIElement;

@end

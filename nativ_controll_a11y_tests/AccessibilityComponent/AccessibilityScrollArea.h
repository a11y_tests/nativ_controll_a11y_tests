//
//  AccessibilityScrollArea.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 19.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//


#import "AccessibilityComponent.h"

@interface AccessibilityScrollArea : AccessibilityComponent

- (NSArray *)initializeAttributeNamesWithEnv;
- (NSArray *)accessibilityContentsAttribute;
- (BOOL)accessibilityIsContentsAttributeSettable;
- (id)accessibilityVerticalScrollBarAttribute;
- (BOOL)accessibilityIsVerticalScrollBarAttributeSettable;
- (id)accessibilityHorizontalScrollBarAttribute;
- (BOOL)accessibilityIsHorizontalScrollBarAttributeSettable;

@end

//
//  AccessibilityTabGroupControl.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 19.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityComponent.h"

@interface AccessibilityTabGroupControl : AccessibilityComponent {
    id fTabGroup;
}

- (id)initWithParent:(NSObject *)parent withAccessible:(id)accessible withIndex:(int)index withTabGroup:(id)tabGroup withView:(NSView *)view withJavaRole:(NSAccessibilityRole)javaRole;
- (id)tabGroup;
- (void)getActionsWithEnv;

- (id)accessibilityValueAttribute;

@end

//
//  AccessibilityTabGroup.h
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 19.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityComponent.h"

@interface AccessibilityTabGroup : AccessibilityComponent {
    NSInteger _numTabs;
}

- (id)currentTabWithEnv;
- (NSArray *)tabControlsWithEnv;
- (NSArray *)contentsWithEnv;
- (NSArray *)initializeAttributeNamesWithEnv;

- (NSArray *)accessibilityArrayAttributeValues:(NSString *)attribute index:(NSUInteger)index maxCount:(NSUInteger)maxCount;
- (NSArray *)accessibilityChildrenAttribute;
- (id) accessibilityTabsAttribute;
- (BOOL)accessibilityIsTabsAttributeSettable;
- (NSArray *)accessibilityContentsAttribute;
- (BOOL)accessibilityIsContentsAttributeSettable;
- (id) accessibilityValueAttribute;

@end

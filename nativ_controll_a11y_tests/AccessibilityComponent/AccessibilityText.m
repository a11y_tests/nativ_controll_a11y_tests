//
//  AccessibilityText.m
//  nativ_controll_a11y_tests
//
//  Created by Артём Семёнов on 18.02.2020.
//  Copyright © 2020 Артём Семёнов. All rights reserved.
//

#import "AccessibilityText.h"
#import "AccessibilityUtilitys.h"

@implementation AccessibilityText

- (NSArray *)initializeAttributeNamesWithEnv {
    static NSArray *attributes = nil;

    if (attributes == nil) {
        //APPKIT_LOCK;
        if (attributes == nil) {
            NSMutableArray *temp = [[super initializeAttributeNamesWithEnv] mutableCopy];
            //[temp removeObject:NSAccessibilityTitleAttribute]; // title may have been set in the superclass implementation - some static text reports from java that it has a name
            [temp addObjectsFromArray:[NSArray arrayWithObjects:
                NSAccessibilityValueAttribute,
                NSAccessibilitySelectedTextAttribute,
                NSAccessibilitySelectedTextRangeAttribute,
                NSAccessibilityNumberOfCharactersAttribute,
                NSAccessibilityVisibleCharacterRangeAttribute,
                NSAccessibilityInsertionPointLineNumberAttribute,
                //    NSAccessibilitySharedTextUIElementsAttribute, // cmcnote: investigate what these two are for. currently unimplemented
                //    NSAccessibilitySharedCharacterRangeAttribute,
                nil]];
            attributes = [[NSArray alloc] initWithArray:temp];
        }
        //APPKIT_UNLOCK;
    }
    return attributes;
}

- (NSString *)accessibilityValueAttribute {
    if ([[self accessibilityRoleAttribute] isEqualToString:NSAccessibilityStaticTextRole]) {
        // if it's static text, the AppKit AXValue is the java accessibleName
        if ([fComponent isKindOfClass:[NSString class]]) {
            return fComponent;
        } else return @"";
    } else return @"";
}

- (BOOL)accessibilityIsValueAttributeSettable
{
    // if text is enabled and editable, it's settable (according to NSCellTextAttributesAccessibility)
    BOOL isEnabled = [(NSNumber *)[self accessibilityEnabledAttribute] boolValue];
    if (!isEnabled) return NO;

    return YES;
}

- (void)accessibilitySetValueAttribute:(id)value {

}

- (NSString *)accessibilitySelectedTextAttribute {
    return @"";
}

- (BOOL)accessibilityIsSelectedTextAttributeSettable {
    return YES; //cmcnote: for AXTextField that's selectable, it's settable. Investigate further.
}

- (void)accessibilitySetSelectedTextAttribute:(id)value
{

    }

/*
- (NSValue *)accessibilitySelectedTextRangeAttribute
{
    return ;
} */

- (BOOL)accessibilityIsSelectedTextRangeAttributeSettable {
    return [(NSNumber *)[self accessibilityEnabledAttribute] boolValue]; // cmcnote: also may want to find out if isSelectable. Investigate.
}

- (void)accessibilitySetSelectedTextRangeAttribute:(id)value {

}

- (NSNumber *)accessibilityNumberOfCharactersAttribute {
    return [NSNumber numberWithLong:[(NSString *)fComponent length]];
}

- (BOOL)accessibilityIsNumberOfCharactersAttributeSettable {
    return NO; // according to NSTextViewAccessibility.m and NSCellTextAttributesAccessibility.m
}

- (BOOL)accessibilityIsVisibleCharacterRangeAttributeSettable {
    return NO;
}

- (NSValue *)accessibilitySelectedTextRangeAttribute {
    return [NSValue valueWithRange:NSRangeFromString(fComponent)];
}

- (NSValue *)accessibilityInsertionPointLineNumberAttribute { 
    return [NSNumber numberWithInt:0];
}

@end
